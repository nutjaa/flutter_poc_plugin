import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:poc_plugin/poc_plugin.dart';

void main() {
  const MethodChannel channel = MethodChannel('poc_plugin');

  TestWidgetsFlutterBinding.ensureInitialized();

  setUp(() {
    channel.setMockMethodCallHandler((MethodCall methodCall) async {
      return '42';
    });
  });

  tearDown(() {
    channel.setMockMethodCallHandler(null);
  });

  test('getPlatformVersion', () async {
    expect(await PocPlugin.platformVersion, '42');
  });
}
