import 'dart:async';

import 'package:flutter/services.dart';

class PocPlugin {
  static const MethodChannel _channel = MethodChannel('poc_plugin');
  static const EventChannel _randomNumberChannel = EventChannel('poc_random_number_channel');

  static Future<String?> get platformVersion async {
    final String? version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }

  static Stream<int> get getRandomNumberStream {
    return _randomNumberChannel.receiveBroadcastStream().cast();
  }
}
