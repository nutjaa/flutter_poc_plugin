import Flutter
import UIKit

public class SwiftPocPlugin: NSObject, FlutterPlugin {
  public static func register(with registrar: FlutterPluginRegistrar) {
    let channel = FlutterMethodChannel(name: "poc_plugin", binaryMessenger: registrar.messenger())
    let randomNumberChannel = FlutterEventChannel(name: "poc_random_number_channel", binaryMessenger: registrar.messenger())
    let instance = SwiftPocPlugin()
    registrar.addMethodCallDelegate(instance, channel: channel)

    let randomNumberStreamHandler = RandomNumberStreamHandler()
    randomNumberChannel.setStreamHandler(randomNumberStreamHandler)
  }

  public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
    result("iOS " + UIDevice.current.systemVersion)
  }
}
